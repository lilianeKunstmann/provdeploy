import os

MONETPORT = 50000
DFANALYZER_PORT = 22000
FLASKPORT = 5000
HOME_PROV_DEPLOY= os.getcwd()
CONTAINER_PROVENANCE="contprov"
DATAFLOW_PROVENANCE="dataprov"
composition={'C':'Coarse-grained', 'H':'Hybrid', 'F':'Fine-grained'}
