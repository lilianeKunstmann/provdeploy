Para executar o de execução com a provDeploy você deverá instalar e configurar os seguintes programas:

- Apptainer
- Python >=2
- Bibliptécas Python: pymonetdb e paramiko

Para as dependências Python aconselhamos o uso de um virtual env.

Você pode executar o arquivo auto_instrumented.py utulizando o arquivo submit.json:

python script.py -s  submit.json


